package it.syncroweb.custom_mapbox_plugin.utilities

interface DeliverButtonTappedCallback {
    fun onEventOccurred()
    fun buttonExitNavigationTap()
}