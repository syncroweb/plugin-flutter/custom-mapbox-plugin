package it.syncroweb.custom_mapbox_plugin

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.SurfaceHolder
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import it.syncroweb.custom_mapbox_plugin.databinding.ActivityScanQrBinding
import it.syncroweb.custom_mapbox_plugin.models.MapBoxEvents
import it.syncroweb.custom_mapbox_plugin.utilities.PluginUtilities
import java.io.IOException

class ScanQrActivity : AppCompatActivity() {
    private lateinit var binding: ActivityScanQrBinding

    private val requestCodeCameraPermission = 1001
    private lateinit var cameraSource: CameraSource
    private lateinit var barcodeDetector: BarcodeDetector
    private var scannedValue = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_AppCompat_NoActionBar)
        binding = ActivityScanQrBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.notesTextField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (binding.notesTextField.text?.isEmpty() == true){
                    binding.hintNotes.visibility = View.VISIBLE
                }
                if (binding.notesTextField.text?.isNotEmpty() == true){
                    binding.hintNotes.visibility = View.INVISIBLE
                }
            }

        })

        if (ContextCompat.checkSelfPermission(
                this@ScanQrActivity, android.Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            askForCameraPermission()
        } else {
            setupControls()
        }
        binding.buttonCancelOperation.setOnClickListener {
            binding.dialogDeliverOrder.visibility = View.INVISIBLE
            binding.buttonBack.visibility = View.VISIBLE
            binding.scanBackground.visibility = View.VISIBLE
        }

        val resultIntent = Intent()
        binding.buttonBack.setOnClickListener {
            binding.buttonBack.isClickable = false
            this.finish()
        }
        binding.buttonConfirmOrder.setOnClickListener {
            binding.dialogDeliverOrder.isClickable = false
            val notes = binding.notesTextField.text.toString()
            resultIntent.putExtra("notes", notes)
            setResult(Activity.RESULT_OK,resultIntent)
            this.finish()
        }

    }
    private fun setupControls() {
        barcodeDetector =
            BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.ALL_FORMATS).build()

        cameraSource = CameraSource.Builder(this, barcodeDetector)
            .setRequestedPreviewSize(1920, 1080)
            .setAutoFocusEnabled(true)
            .build()

        binding.cameraSurfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            @SuppressLint("MissingPermission")
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    //Start preview after 1s delay
                    cameraSource.start(holder)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            @SuppressLint("MissingPermission")
            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) {
                try {
                    cameraSource.start(holder)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource.stop()
            }
        })

        barcodeDetector.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                Toast.makeText(applicationContext, "Scanner has been closed", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun receiveDetections(detections: Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() == 1) {
                    scannedValue = barcodes.valueAt(0).rawValue
                    runOnUiThread {
                        if (scannedValue == FlutterMapboxNavigationPlugin.code){
                            binding.dialogDeliverOrder.visibility = View.VISIBLE
                            binding.buttonBack.visibility = View.INVISIBLE
                            binding.scanBackground.visibility = View.INVISIBLE
                        }
                    }
                }
            }
        })
    }

    private fun askForCameraPermission() {
        ActivityCompat.requestPermissions(
            this@ScanQrActivity,
            arrayOf(android.Manifest.permission.CAMERA),
            requestCodeCameraPermission
        )
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCodeCameraPermission && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupControls()
            } else {
                Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        cameraSource.stop()
    }
}